import React from 'react';

class Header extends React.Component{
  render() {
    return (
      <header style={{
        textAlign: "center",
        color: "#262626",
        padding:"10px"
      }}>
        <h1>Tic - Tac - Toe</h1>
      </header>
    )
  }
}

export default Header;